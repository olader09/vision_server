class CreatePersons < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :mood, default: ''
      t.float :confidence_mood, default: 0.0
      t.integer :age, default: 0
      t.string :gender, default: ''
      t.float :confidence_gender, default: 0.0
      t.string :photo
      t.timestamps
    end
  end
end
