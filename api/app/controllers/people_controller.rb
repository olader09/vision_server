class PeopleController < APIBaseController

  def index; end

  def show; end

  def update; end

  def create
    person = Person.create(create_person_params)
    if person.errors.blank?
      ActionCable.server.broadcast "room_channel_1", mood: person.mood, age: person.age, gender: person.gender, photo: person.photo, confidence_mood: person.confidence_mood, confidence_gender: person.confidence_gender
      render json: person, status: :ok
    else
      render json: person.errors, status: :bad_request
    end
  end

  def destroy; end

  protected

  def default_person_fields
    %i[mood confidence_mood age gender confidence_gender photo]
  end

  def update_person_params
    params.required(:person).permit(
      *default_person_fields
    )
  end

  def create_person_params
    params.required(:person).permit(
      *default_person_fields
    )
  end
end
