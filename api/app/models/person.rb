class Person < ApplicationRecord
  enum mood: {
    "Счастлив": 'happy',
    "Грустный": 'sad',
    "Нейтральное": 'neutral',
    "Удивленное": 'surprise',
    "Страшно": 'fear'
  }
  enum gender: {
    "Женщина": 'F',
    "Мужчина": 'M'
  }
  mount_uploader :photo, PhotoUploader
  mount_base64_uploader :photo, PhotoUploader

  validates :mood, presence: true
end
