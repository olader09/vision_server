class RoomChannel < ApplicationCable::Channel

  def subscribed
    stream_from "room_channel_#{params['chat_id']}"
  end

  def unsubscribed
  
  end

  def receive(data)

  end
end
